package com.fourcatsdev.aula05.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula05.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
